<?php

namespace Drupal\ckeditor_quran\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "quran" plugin.
 *
 * @CKEditorPlugin(
 *   id = "quran",
 *   label = @Translation("Quran")
 * )
 */
 
 class ckeditor_quran extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return 'libraries/quran/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'Chart' => [
        'label' => t('Chart'),
        'image' => 'libraries/quran/icons/chart.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
